import angular from 'angular';

class CountryService {
	constructor($http) {
		"ngInject";

		this.$http = $http;

		this.url = 'http://localhost:8080/countries';
	}

	getCountries() {
		return this.$http.get(this.url);

	}

	addCountry(country) {
		return this.$http.post(this.url, country);

	}

	updateCountry(country){
		return this.$http.put(this.url+'/'+country.id, country);

	}

	deleteCountry(id){
		return this.$http.delete(this.url+'/'+id);
	}
}

const countryService = angular.module('countryService', [])
.service('CountryService', CountryService)
.name;

export default countryService;
