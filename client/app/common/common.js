import angular from 'angular';
import Modal from './modal/modal';

let commonModule = angular.module('app.common', [
  Modal,
]).name;

export default commonModule;
