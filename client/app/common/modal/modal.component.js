import template from './modal.html';
import controller from './modal.controller';
import './modal.scss';

let modalComponent = {
  bindings: {
  	modalId: '@',
  	template: '@',
  	scope: '<'
  },
  template,
  controller
};

export default modalComponent;
