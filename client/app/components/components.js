import angular from 'angular';
import CountryPage from './country/country';

let componentModule = angular.module('app.components', [
  CountryPage
])

.name;

export default componentModule;
