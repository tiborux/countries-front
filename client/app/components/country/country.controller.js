class CountryController {
	constructor(CountryService) {
		"ngInject";

		this.CountryService = CountryService;

		this.country = {};
		this.countries = [];
		this.isEdit = false;
	}

	refresh() {
		this.CountryService.getCountries().then((data) => {
			this.countries = data.data;
		});
	}

	add() {
		if(!this.isEdit){
			if(this.country.countryName && this.country.population){
				this.CountryService.addCountry(this.country).then((data) => {
					this.countries.push(data.data);
					this.country = {};
					$('#addCountryModal').modal('hide');
				});
			}
		} else {
			this.CountryService.updateCountry(this.country).then((data) => {
				var country = this.countries.findIndex((data)=> data.id == this.country.id);
				this.countries[country]=this.country;
				this.isEdit = false;
				$('#addCountryModal').modal('hide');

			});
		}

	}

	edit(country) {
		this.isEdit = true;
		this.country = Object.assign({}, country);
	}

	delete() {
		this.CountryService.deleteCountry(this.country.id).then((data) => {
			this.countries = this.countries.filter((item) => { return item.id != this.country.id })
			this.country = {};
		});

	}

	selectedCountry(country){
		this.country = Object.assign({}, country);
	}

	resetStatus(){
		this.isEdit = false;
		this.country = {};
	}
}

export default CountryController;
