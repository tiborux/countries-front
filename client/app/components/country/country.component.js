import template from './country.html';
import controller from './country.controller';
import './country.scss';

let countryComponent = {
  bindings: {},
  template,
  controller
};

export default countryComponent;
