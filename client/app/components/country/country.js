import angular from 'angular';
import uiRouter from 'angular-ui-router';
import countryComponent from './country.component';

let countryModule = angular.module('country', [
  uiRouter
])

.config(($stateProvider, $urlRouterProvider) => {
  "ngInject";

  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('country', {
      url: '/',
      component: 'country'
    });
})

.component('country', countryComponent)

.name;

export default countryModule;
